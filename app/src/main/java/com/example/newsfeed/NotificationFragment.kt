package com.example.newsfeed

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.example.newsfeed.adapter.PostAdapter
import com.example.newsfeed.adapter.PostListAdapter
import com.example.newsfeed.adapter.PostNotiAdapter
import com.example.newsfeed.model.Post
import com.example.newsfeed.model.PostList
import com.example.newsfeed.model.PostNoification
import kotlinx.android.synthetic.main.fragment_newsfeed.*
import kotlinx.android.synthetic.main.notification.*

class NotificationFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.notification, container, false)
    }
    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val postsNofication: ArrayList<PostNoification> = ArrayList()
        for (i in 0..10) {
            postsNofication.add(
                PostNoification(
                    "https://picsum.photos/200/300?random&$i",
                    "Linh Tran$i đã nhắc đến bạn trong một bình luận","15 phút trước"
                )
            )
        }
        val adapterNotificationFragment =
            PostNotiAdapter(postsNofication, requireContext())

        recyclerView_notification.adapter = adapterNotificationFragment
        recyclerView_notification.layoutManager =
            LinearLayoutManager(requireContext(), OrientationHelper.VERTICAL, false)


    }

}