package com.example.newsfeed.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfeed.model.PostList
import com.example.newsfeed.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_row.view.*

class PostListAdapter(private val postslist: ArrayList<PostList>, private val context: Context) :
    RecyclerView.Adapter<PostListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = postslist.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = postslist[position]
        holder.textView1.text = currentItem.username
        holder.textView2.text = currentItem.time
        holder.textView3.text = currentItem.words
        Picasso.with(context).load(currentItem.avatar).into(holder.imageView1)
        Picasso.with(context).load(currentItem.photo).into(holder.imageView2)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView1: ImageView = itemView.avatar_postslist
        val textView1: TextView = itemView.text_name
        val textView2: TextView = itemView.text_time
        val textView3: TextView = itemView.text_words
        val imageView2: ImageView = itemView.photo_postslist
    }
}