package com.example.newsfeed.model

data class PostNoification(val photo: String, val content: String, val time: String)