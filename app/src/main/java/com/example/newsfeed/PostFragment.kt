 package com.example.newsfeed

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.example.newsfeed.adapter.PostAdapter
import com.example.newsfeed.adapter.PostListAdapter
import com.example.newsfeed.model.Post
import com.example.newsfeed.model.PostList
import kotlinx.android.synthetic.main.fragment_newsfeed.*

class PostFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_newsfeed, container, false)
    }

    @SuppressLint("WrongConstant")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val posts: ArrayList<Post> = ArrayList()
        for (i in 0..10) {
            posts.add(
                Post(
                    "https://picsum.photos/200/300?random&$i",
                    "Linh Tran$i"
                )
            )
        }
        val adapter =
            PostAdapter(posts, requireContext())

        val postslist: ArrayList<PostList> = ArrayList()
        for (i in 0..10) {
            postslist.add(
                PostList(
                    "https://picsum.photos/200/300?random&$i",
                    "Linh Tran$i",
                    "December 16 at 10:10",
                    "This is a picture",
                    "https://picsum.photos/900/700  ?random&$i"
                )
            )
        }
        val adapterlist = PostListAdapter(
            postslist,
            requireContext()
        )

        recyclerView.adapter = adapter
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), OrientationHelper.HORIZONTAL, false)

        recyclerView_2.adapter = adapterlist
        recyclerView_2.layoutManager = LinearLayoutManager(requireContext())

        view.findViewById<Button>(R.id.create_one_post).setOnClickListener {
            findNavController().navigate(R.id.action_postFragment_to_onePostFragment)
        }
        view.findViewById<Button>(R.id.notification).setOnClickListener {
            findNavController().navigate(R.id.action_postFragment_to_notificationFragment)
        }


        fun insertItem (view: View){
            val index : Int = 0;
            val newItem = PostList("https://picsum.photos/200/300?random&"," Linh Trần ",
                "December 16 at 10:10","This is new post", "https://picsum.photos/900/700  ?random&")
        }
    }
}