package com.example.newsfeed.model

data class PostList(val avatar:String, val username:String, val time:String, val words:String, val photo:String)