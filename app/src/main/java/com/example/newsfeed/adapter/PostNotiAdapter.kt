package com.example.newsfeed.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfeed.R
import com.example.newsfeed.model.PostNoification
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_row.view.*
import kotlinx.android.synthetic.main.notification_item.view.*

class PostNotiAdapter (private val postsNotification: ArrayList<PostNoification>, private val context: Context) :
    RecyclerView.Adapter<PostNotiAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostNotiAdapter.ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false)
        return PostNotiAdapter.ViewHolder(itemView)
    }

    override fun getItemCount() = postsNotification.size
    override fun onBindViewHolder(holder: PostNotiAdapter.ViewHolder, position: Int) {
        val currentItem = postsNotification[position]
        holder.textView2.text = currentItem.time
        holder.textView1.text = currentItem.content
        Picasso.with(context).load(currentItem.photo).into(holder.imageView1)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView1: ImageView = itemView.avatar_notification
        val textView1: TextView = itemView.text_contentNotification
        val textView2: TextView = itemView.text_timeNotification


    }
}
