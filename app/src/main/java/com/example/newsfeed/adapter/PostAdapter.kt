package com.example.newsfeed.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfeed.model.Post
import com.example.newsfeed.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.story_row.view.*

class PostAdapter(private val posts: ArrayList<Post>, private val context: Context) :
    RecyclerView.Adapter<PostAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.story_row, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = posts[position]
        holder.textView.text = currentItem.username
        Picasso.with(context).load(currentItem.photo).into(holder.imageView)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.user_avatar
        val textView: TextView = itemView.user_name
    }

}