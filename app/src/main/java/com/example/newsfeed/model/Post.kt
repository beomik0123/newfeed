package com.example.newsfeed.model

data class Post(val photo: String, val username: String)